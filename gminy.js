$('#myModal').on('show.bs.modal', function (e) {
  if (window.innerWidth > 576) {
    return e.preventDefault();
  }
}).modal('show');


$( "#loading" ).css("display","inline-block");	

$.post('/PHP/gminy5.php',	function(data)  {
var d2 = $.Deferred();
  var paths;
  var formData;
var minludnosc_na1km;
var maxludnosc_na1km;  
var mindochody_na1os;
var maxdochody_na1os;


    var engine = new Bloodhound({
        datumTokenizer: function (datum) {
            return Bloodhound.tokenizers.whitespace(datum.nazwa);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
 local: data.symbteryt

    });


    // instantiate the typeahead UI
    $('#powiat').typeahead(
      {
          hint: true,
          highlight: true,
          minLength: 3
      },
      {
          name: 'engine',
      display: function(item){ return item.nazwa+' - '+item.WOJ+item.POW+item.GMI+item.RODZ},
	  
          source: engine,
        
		   limit: 14
      });
	  	  
		  
     var border=1;
 
    var svg = d3.select('#wrapper').append("svg");

		
    var projection = d3.geoMercator();

    
    var path = d3.geoPath().projection(projection);
      
var color = d3.scaleQuantize();
					
var startflag=true; 
var kodwoj='24';						
var kodwojnew = 'start'; 

	$("#form1").on( "submit", function(){

$( "#loading" ).css("display","inline-block");	
	
var urljson;
var topologyname;



 	var input=$('#powiat').val().split(" - ");
	if (input[1]) {
	kodwojnew=input[1].substr(0,2);
	formData = {kod: input[1]};
}

if (!kodwojnew) {$( "#loading" ).css("display","none"); return false;}



if (kodwojnew && kodwoj!=kodwojnew) {

d3.selectAll('g').remove();
 
		svg.append("g");
	

switch (kodwojnew) {
  case 'start':
  case '24':
    urljson ='/mapy/slaskpowgmirazem3.json';
    break;
  case '14':
    urljson='/mapy/mazowpowgmirazem2.json';
    break;
  case '10':
    urljson='/mapy/lodzpowgmirazem.json';
    break;
  case '26':
    urljson='/mapy/swkrzyspowgmirazem.json';
    break;
  case '22':
   urljson='/mapy/pomopowgmirazem.json';
    break;
  case '04':
    urljson='/mapy/kujpompowgmirazem.json';
    break;
  case '12':
    urljson='/mapy/malpolpowgmirazem.json';
    break;
  case '28':
    urljson='/mapy/warmazpowgmirazem.json';
    break;
  case '30':
    urljson='/mapy/wielpolpowgmirazem.json';
    break;
  case '16':
    urljson='/mapy/opolpowgmirazem.json';
    break;  
  case '32':
    urljson='/mapy/zachpompowgmirazem.json';
    break;  
  case '08':
    urljson='/mapy/lubuspowgmirazem.json';
	break;
  case '02':
    urljson='/mapy/dolslpowgmirazem.json';
    break;
  case '18':
    urljson='/mapy/podkarpowgmirazem.json';
    break;
  case '06':
    urljson='/mapy/lubelpowgmirazem.json';
	break;
  case '20':
    urljson='/mapy/podlaspowgmirazem.json';
}


d3.json(urljson, function(error, topology) {
  if (error) throw error;
  
 var wojmap;
var featureCollection;

if (kodwojnew=='start' || kodwojnew=='24') { 
	$("#tytwoj").html('woj. śląskie');
	projection.center([19.6, 50.5]).scale(15000);
	
	 wojmap = svg.select("g").selectAll("g")
	.data(topojson.feature(topology, topology.objects.slaskpowgmirazem3).features)
	.enter()
	.append("g");
	 featureCollection = topojson.feature(topology, topology.objects.slaskpowgmirazem3).features;

svg.select("g").append("path")
	  .attr("class", "borderpowiaty")
.attr("d", path(topojson.mesh(topology, topology.objects.slaskpowgmirazem3, function(a, b) { return a.properties.layer === 'Powiaty' || a.properties.layer === 'Województwa';})));						

svg.select("g").append("path")
	  .attr("class", "bordergminy")
.attr("d", path(topojson.mesh(topology, topology.objects.slaskpowgmirazem3, function(a, b) { return a.properties.layer === 'Gminy'; })));		 
	
	}
	
else if (kodwojnew=='14') {
	$("#tytwoj").html('woj. mazowiecke');
	projection.center([21.6, 52.7]).scale(10000);

	wojmap = svg.select("g").selectAll("g")
	.data(topojson.feature(topology, topology.objects.mazowpowgmirazem).features)
	.enter()
	.append("g");
	 featureCollection = topojson.feature(topology, topology.objects.mazowpowgmirazem).features;

svg.select("g").append("path")
	  .attr("class", "borderpowiaty")
.attr("d", path(topojson.mesh(topology, topology.objects.mazowpowgmirazem, function(a, b) { return a.properties.layer === 'Powiaty' || a.properties.layer === 'Województwa';})));						

svg.select("g").append("path")
	  .attr("class", "bordergminy")
.attr("d", path(topojson.mesh(topology, topology.objects.mazowpowgmirazem, function(a, b) { return a.properties.layer === 'Gminy'; })));		
	
	 }
 
else if (kodwojnew=='10') {
	$("#tytwoj").html('woj. łódzkie');
	projection.center([20.1, 51.8]).scale(13000);

	wojmap = svg.select("g").selectAll("g")
	.data(topojson.feature(topology, topology.objects.lodzpowgmirazem).features)
	.enter()
	.append("g");
	 featureCollection = topojson.feature(topology, topology.objects.lodzpowgmirazem).features;

svg.select("g").append("path")
	  .attr("class", "borderpowiaty")
.attr("d", path(topojson.mesh(topology, topology.objects.lodzpowgmirazem, function(a, b) { return a.properties.layer === 'Powiaty' || a.properties.layer === 'Województwa';})));						

svg.select("g").append("path")
	  .attr("class", "bordergminy")
.attr("d", path(topojson.mesh(topology, topology.objects.lodzpowgmirazem, function(a, b) { return a.properties.layer === 'Gminy'; })));		
	
	 } 

else if (kodwojnew=='26') {
	$("#tytwoj").html('woj. świętokrzyskie');
	projection.center([21.5, 50.8]).scale(13000);

	wojmap = svg.select("g").selectAll("g")
	.data(topojson.feature(topology, topology.objects.swkrzyspowgmirazem).features)
	.enter()
	.append("g");
	 featureCollection = topojson.feature(topology, topology.objects.swkrzyspowgmirazem).features;

svg.select("g").append("path")
	  .attr("class", "borderpowiaty")
.attr("d", path(topojson.mesh(topology, topology.objects.swkrzyspowgmirazem, function(a, b) { return a.properties.layer === 'Powiaty' || a.properties.layer === 'Województwa';})));						

svg.select("g").append("path")
	  .attr("class", "bordergminy")
.attr("d", path(topojson.mesh(topology, topology.objects.swkrzyspowgmirazem, function(a, b) { return a.properties.layer === 'Gminy'; })));		
	
	 } 

else if (kodwojnew=='22') {
	$("#tytwoj").html('woj. pomorskie');
	projection.center([18.6, 54.4]).scale(13000);

	wojmap = svg.select("g").selectAll("g")
	.data(topojson.feature(topology, topology.objects.pomopowgmirazem).features)
	.enter()
	.append("g");
	 featureCollection = topojson.feature(topology, topology.objects.pomopowgmirazem).features;

svg.select("g").append("path")
	  .attr("class", "borderpowiaty")
.attr("d", path(topojson.mesh(topology, topology.objects.pomopowgmirazem, function(a, b) { return a.properties.layer === 'Powiaty' || a.properties.layer === 'Województwa';})));						

svg.select("g").append("path")
	  .attr("class", "bordergminy")
.attr("d", path(topojson.mesh(topology, topology.objects.pomopowgmirazem, function(a, b) { return a.properties.layer === 'Gminy'; })));		
	
	 }	 
	 
else if (kodwojnew=='04') {
	$("#tytwoj").html('woj. kujawsko-pomorskie');
	projection.center([18.9, 53.3]).scale(13000);

	wojmap = svg.select("g").selectAll("g")
	.data(topojson.feature(topology, topology.objects.kujpompowgmirazem).features)
	.enter()
	.append("g");
	 featureCollection = topojson.feature(topology, topology.objects.kujpompowgmirazem).features;

svg.select("g").append("path")
	  .attr("class", "borderpowiaty")
.attr("d", path(topojson.mesh(topology, topology.objects.kujpompowgmirazem, function(a, b) { return a.properties.layer === 'Powiaty' || a.properties.layer === 'Województwa';})));						

svg.select("g").append("path")
	  .attr("class", "bordergminy")
.attr("d", path(topojson.mesh(topology, topology.objects.kujpompowgmirazem, function(a, b) { return a.properties.layer === 'Gminy'; })));		
	
	 }	 
	 
else if (kodwojnew=='12') {
	$("#tytwoj").html('woj. małopolskie');
	projection.center([21.0, 50.0]).scale(13000);

	wojmap = svg.select("g").selectAll("g")
	.data(topojson.feature(topology, topology.objects.malpolpowgmirazem).features)
	.enter()
	.append("g");
	 featureCollection = topojson.feature(topology, topology.objects.malpolpowgmirazem).features;

svg.select("g").append("path")
	  .attr("class", "borderpowiaty")
.attr("d", path(topojson.mesh(topology, topology.objects.malpolpowgmirazem, function(a, b) { return a.properties.layer === 'Powiaty' || a.properties.layer === 'Województwa';})));						

svg.select("g").append("path")
	  .attr("class", "bordergminy")
.attr("d", path(topojson.mesh(topology, topology.objects.malpolpowgmirazem, function(a, b) { return a.properties.layer === 'Gminy'; })));		
	
	 }

else if (kodwojnew=='28') {
	$("#tytwoj").html('woj. warmińsko-mazurskie');
	projection.center([20.9, 53.9]).scale(13000);

	wojmap = svg.select("g").selectAll("g")
	.data(topojson.feature(topology, topology.objects.warmazpowgmirazem).features)
	.enter()
	.append("g");
	 featureCollection = topojson.feature(topology, topology.objects.warmazpowgmirazem).features;

svg.select("g").append("path")
	  .attr("class", "borderpowiaty")
.attr("d", path(topojson.mesh(topology, topology.objects.warmazpowgmirazem, function(a, b) { return a.properties.layer === 'Powiaty' || a.properties.layer === 'Województwa';})));						

svg.select("g").append("path")
	  .attr("class", "bordergminy")
.attr("d", path(topojson.mesh(topology, topology.objects.warmazpowgmirazem, function(a, b) { return a.properties.layer === 'Gminy'; })));		
	
	 }

else if (kodwojnew=='30') {
	$("#tytwoj").html('woj. wielkopolskie');
	projection.center([17.8, 52.6]).scale(11000);

	wojmap = svg.select("g").selectAll("g")
	.data(topojson.feature(topology, topology.objects.wielpolpowgmirazem).features)
	.enter()
	.append("g");
	 featureCollection = topojson.feature(topology, topology.objects.wielpolpowgmirazem).features;

svg.select("g").append("path")
	  .attr("class", "borderpowiaty")
.attr("d", path(topojson.mesh(topology, topology.objects.wielpolpowgmirazem, function(a, b) { return a.properties.layer === 'Powiaty' || a.properties.layer === 'Województwa';})));						

svg.select("g").append("path")
	  .attr("class", "bordergminy")
.attr("d", path(topojson.mesh(topology, topology.objects.wielpolpowgmirazem, function(a, b) { return a.properties.layer === 'Gminy'; })));		
	
	 }
	 
else if (kodwojnew=='16') {
	$("#tytwoj").html('woj. opolskie');
	projection.center([18.6, 50.8]).scale(15000);

	wojmap = svg.select("g").selectAll("g")
	.data(topojson.feature(topology, topology.objects.opolpowgmirazem).features)
	.enter()
	.append("g");
	 featureCollection = topojson.feature(topology, topology.objects.opolpowgmirazem).features;

svg.select("g").append("path")
	  .attr("class", "borderpowiaty")
.attr("d", path(topojson.mesh(topology, topology.objects.opolpowgmirazem, function(a, b) { return a.properties.layer === 'Powiaty' || a.properties.layer === 'Województwa';})));						

svg.select("g").append("path")
	  .attr("class", "bordergminy")
.attr("d", path(topojson.mesh(topology, topology.objects.opolpowgmirazem, function(a, b) { return a.properties.layer === 'Gminy'; })));		
	
	 }	 
	 
else if (kodwojnew=='32') {
	$("#tytwoj").html('woj. zachodniopomorskie');
	projection.center([15.8, 53.6]).scale(13000);

	wojmap = svg.select("g").selectAll("g")
	.data(topojson.feature(topology, topology.objects.zachpompowgmirazem).features)
	.enter()
	.append("g");
	 featureCollection = topojson.feature(topology, topology.objects.zachpompowgmirazem).features;

svg.select("g").append("path")
	  .attr("class", "borderpowiaty")
.attr("d", path(topojson.mesh(topology, topology.objects.zachpompowgmirazem, function(a, b) { return a.properties.layer === 'Powiaty' || a.properties.layer === 'Województwa';})));						

svg.select("g").append("path")
	  .attr("class", "bordergminy")
.attr("d", path(topojson.mesh(topology, topology.objects.zachpompowgmirazem, function(a, b) { return a.properties.layer === 'Gminy'; })));		
	
	 }	
	 
else if (kodwojnew=='08') {
	$("#tytwoj").html('woj. lubuskie');
	projection.center([16.2, 52.4]).scale(14000);

	wojmap = svg.select("g").selectAll("g")
	.data(topojson.feature(topology, topology.objects.lubuspowgmirazem).features)
	.enter()
	.append("g");
	 featureCollection = topojson.feature(topology, topology.objects.lubuspowgmirazem).features;

svg.select("g").append("path")
	  .attr("class", "borderpowiaty")
.attr("d", path(topojson.mesh(topology, topology.objects.lubuspowgmirazem, function(a, b) { return a.properties.layer === 'Powiaty' || a.properties.layer === 'Województwa';})));						

svg.select("g").append("path")
	  .attr("class", "bordergminy")
.attr("d", path(topojson.mesh(topology, topology.objects.lubuspowgmirazem, function(a, b) { return a.properties.layer === 'Gminy'; })));		
	
	 }	

else if (kodwojnew=='02') {
	$("#tytwoj").html('woj. dolnośląskie');
	projection.center([16.9, 51.2]).scale(13000);

	wojmap = svg.select("g").selectAll("g")
	.data(topojson.feature(topology, topology.objects.dolslpowgmirazem).features)
	.enter()
	.append("g");
	 featureCollection = topojson.feature(topology, topology.objects.dolslpowgmirazem).features;

svg.select("g").append("path")
	  .attr("class", "borderpowiaty")
.attr("d", path(topojson.mesh(topology, topology.objects.dolslpowgmirazem, function(a, b) { return a.properties.layer === 'Powiaty' || a.properties.layer === 'Województwa';})));						

svg.select("g").append("path")
	  .attr("class", "bordergminy")
.attr("d", path(topojson.mesh(topology, topology.objects.dolslpowgmirazem, function(a, b) { return a.properties.layer === 'Gminy'; })));		
	
	 }	
	 
else if (kodwojnew=='18') {
	$("#tytwoj").html('woj. podkarpackie');
	projection.center([23.1, 50.1]).scale(13000);

	wojmap = svg.select("g").selectAll("g")
	.data(topojson.feature(topology, topology.objects.podkarpowgmirazem).features)
	.enter()
	.append("g");
	 featureCollection = topojson.feature(topology, topology.objects.podkarpowgmirazem).features;

svg.select("g").append("path")
	  .attr("class", "borderpowiaty")
.attr("d", path(topojson.mesh(topology, topology.objects.podkarpowgmirazem, function(a, b) { return a.properties.layer === 'Powiaty' || a.properties.layer === 'Województwa';})));						

svg.select("g").append("path")
	  .attr("class", "bordergminy")
.attr("d", path(topojson.mesh(topology, topology.objects.podkarpowgmirazem, function(a, b) { return a.properties.layer === 'Gminy'; })));		
	
	 }

else if (kodwojnew=='06') {
	$("#tytwoj").html('woj. lubelskie');
	projection.center([23.6, 51.4]).scale(13000);

	wojmap = svg.select("g").selectAll("g")
	.data(topojson.feature(topology, topology.objects.lubelpowgmirazem).features)
	.enter()
	.append("g");
	 featureCollection = topojson.feature(topology, topology.objects.lubelpowgmirazem).features;

svg.select("g").append("path")
	  .attr("class", "borderpowiaty")
.attr("d", path(topojson.mesh(topology, topology.objects.lubelpowgmirazem, function(a, b) { return a.properties.layer === 'Powiaty' || a.properties.layer === 'Województwa';})));						

svg.select("g").append("path")
	  .attr("class", "bordergminy")
.attr("d", path(topojson.mesh(topology, topology.objects.lubelpowgmirazem, function(a, b) { return a.properties.layer === 'Gminy'; })));		
	
	 }
	 
else if (kodwojnew=='20') {
	$("#tytwoj").html('woj. podlaskie');
	projection.center([23.6, 53.5]).scale(13000);

	wojmap = svg.select("g").selectAll("g")
	.data(topojson.feature(topology, topology.objects.podlaspowgmirazem).features)
	.enter()
	.append("g");
	 featureCollection = topojson.feature(topology, topology.objects.podlaspowgmirazem).features;

svg.select("g").append("path")
	  .attr("class", "borderpowiaty")
.attr("d", path(topojson.mesh(topology, topology.objects.podlaspowgmirazem, function(a, b) { return a.properties.layer === 'Powiaty' || a.properties.layer === 'Województwa';})));						

svg.select("g").append("path")
	  .attr("class", "bordergminy")
.attr("d", path(topojson.mesh(topology, topology.objects.podlaspowgmirazem, function(a, b) { return a.properties.layer === 'Gminy'; })));		
	
	 }

	 
	 paths = wojmap.append("path")
.attr("d", path);
paths
.attr("class", "path");

   
  var zoom = d3.zoom()
  .scaleExtent([.7, 2.1])
   .translateExtent([[-500,-300],[1200,650]])
   .extent([[-250,-150],[600,325]]) 
  .on('zoom', zoomFn);

  

var gElem = d3.select('svg').call(zoom); 
 gElem.call(zoom.transform, d3.zoomIdentity); 

 $.post('/PHP/1tabgminypstfin5.php', formData,	function(data)  {

 minludnosc_na1km =d3.min(data.gminy, function(d) { return parseFloat(d.ludnosc_na1km)});
 maxludnosc_na1km =d3.max(data.gminy, function(d) { return parseFloat(d.ludnosc_na1km)});
mindochody_na1os=d3.min(data.gminy, function(d) { return parseFloat(d.doch_ogolem)});
maxdochody_na1os=d3.max(data.gminy, function(d) { return parseFloat(d.doch_ogolem)});
minwydatki_na1os=d3.min(data.gminy, function(d) { return parseFloat(d.wyd_ogolem)});
maxwydatki_na1os=d3.max(data.gminy, function(d) { return parseFloat(d.wyd_ogolem)});


 for (var i = 0; i < data.gminy.length; i++) {
				
						var dataGMIkod = data.gminy[i].id_gminy;
						var dataGMIpow = data.gminy[i].powierzchnia_km;
						var dataGMIludog =data.gminy[i].ludnosc_og;
						var dataGMIlud1km =data.gminy[i].ludnosc_na1km;
						var dataGMIdochogna1os=data.gminy[i].doch_ogolem;
						
						
						var dataGMIwydogna1os=data.gminy[i].wyd_ogolem;
						var dataGMIwydoswiwych=data.gminy[i].oswiataiwych;
						
						for (var j = 0; j < featureCollection.length; j++) {
						
							var jsonGMI = featureCollection[j].properties.JPT_KOD_JE;
							var jsonGMIPOw= jsonGMI.substr(0,4);
							
							if (dataGMIkod == jsonGMI && jsonGMI.length==7) {
						
							featureCollection[j].properties.dataGMIpow= dataGMIpow;
							
							featureCollection[j].properties.dataGMIludog= dataGMIludog;
							featureCollection[j].properties.dataGMIlud1km= dataGMIlud1km;
							featureCollection[j].properties.dataGMIdochogna1os= dataGMIdochogna1os;
														
							featureCollection[j].properties.dataGMIwydogna1os= dataGMIwydogna1os;
							featureCollection[j].properties.dataGMIwydoswiwych= dataGMIwydoswiwych;
							break;
						
							}
							else if (dataGMIkod === jsonGMIPOw) {
						
							featureCollection[j].properties.dataGMIPOWnazwa= data.gminy[i].nazwa;
							featureCollection[j].properties.dataGMIPOWpow= dataGMIpow;							
							featureCollection[j].properties.dataGMIPOWludog= dataGMIludog;
							featureCollection[j].properties.dataGMIPOWlud1km= dataGMIlud1km;
							featureCollection[j].properties.dataGMIPOWdochogna1os= dataGMIdochogna1os;
							
							featureCollection[j].properties.dataGMIPOWwydogna1os= dataGMIwydogna1os;
							featureCollection[j].properties.dataGMIPOWwydoswiwych= dataGMIwydoswiwych;
									
							}
						}
					}						
						
wojmap.on("mouseover", function(d) { 
			var POWnazwa = d.properties.dataGMIPOWnazwa;
			var GMInr = d.properties.JPT_KOD_JE;
			var	GMInazwa=d.properties.JPT_NAZWA_;
			
			 var radselval = $("input[name='gridRadios']:checked").val();
        if (radselval=='gestosc') { 
			var POWludog=d.properties.dataGMIPOWludog;
			var	GMIpow=d.properties.dataGMIpow;
			var GMIludog=d.properties.dataGMIludog;
			var GMIlud1km=d.properties.dataGMIlud1km;
			if (POWnazwa!=GMInazwa) {
			wysw='Powiat: '+POWnazwa+'<br>POW ludność: '+POWludog+' os<br>Gmina: <span class="text-dark bg-light">&nbsp;'+GMInazwa +
			'&nbsp;</span><br>Kod gminy: '+GMInr+'<br>GMI ludność: '+GMIludog+
			' os<br>GMI ludność: ' +GMIlud1km +' os/km<sup>2</sup><br>GMI powierzchnia: '+GMIpow+' km<sup>2</sup>';
			} else {
			wysw='Miasto na prawach powiatu<br>Gmina miejska: <span class="text-dark bg-light">&nbsp;'+GMInazwa
			+'&nbsp;</span><br>Kod gminy: '+GMInr+ '<br>GMI ludność: '+GMIludog+
			' os<br>GMI ludność: ' +GMIlud1km +' os/km<sup>2</sup><br>GMI powierzchnia: '+GMIpow+' km<sup>2</sup>';
			}	
			} else if (radselval=="dochody"){
			var POWdochogna1os = d.properties.dataGMIPOWdochogna1os;			
			var GMIdochogna1os=d.properties.dataGMIdochogna1os;
				if (POWnazwa!=GMInazwa) {
			wysw='Powiat: '+POWnazwa+'<br>POW dochody na 1miesz. '+POWdochogna1os+' zł<br>Gmina: <span class="text-dark bg-light">&nbsp;'+GMInazwa +
			'&nbsp;</span><br>Kod gminy: '+GMInr+'<br>GMI dochody na 1miesz. '+GMIdochogna1os+' zł';}
				else {
				wysw='Miasto na prawach powiatu<br>Gmina miejska: <span class="text-dark bg-light">&nbsp;'+GMInazwa +
				'&nbsp;</span><br>Kod gminy: '+GMInr+'<br>GMI dochody na 1miesz. '+GMIdochogna1os+' zł';
				}
			} else if (radselval=="wydatki"){
				var POWwydogna1os = d.properties.dataGMIPOWwydogna1os;			
				var GMIwydogna1os = d.properties.dataGMIwydogna1os;			
			
				var POWwydoswiwych=d.properties.dataGMIPOWwydoswiwych;
				var GMIwydoswiwych=d.properties.dataGMIwydoswiwych;
				
				if (POWnazwa!=GMInazwa) {
			wysw='Powiat: '+POWnazwa+'<br>POW wydatki na 1miesz. '+POWwydogna1os+' zł<br>POW wydatki oświata i wych. '+POWwydoswiwych+
			' zł<br>Gmina: <span class="text-dark bg-light">&nbsp;'+GMInazwa +'&nbsp;</span><br>Kod gminy: '+
				GMInr+'<br>GMI wydatki na 1miesz. '+GMIwydogna1os+' zł<br>GMI wyd oświata i wych. '+
				GMIwydoswiwych;
				}
				else {
				wysw='Miasto na prawach powiatu<br>Gmina miejska: <span class="text-dark bg-light">&nbsp;'+POWnazwa+'&nbsp;</span><br>Kod gminy: '+GMInr+
				'<br>POW wydatki na 1miesz. '+POWwydogna1os+' zł<br>POW wydatki oświata i wych. '+POWwydoswiwych+' zł';
				}
			}	
				else {
				
				var POWludog=d.properties.dataGMIPOWludog;
				var POWdochogna1os = d.properties.dataGMIPOWdochogna1os;			
				
				var	GMIpow=d.properties.dataGMIpow;
				var GMIludog=d.properties.dataGMIludog;
				var GMIlud1km=d.properties.dataGMIlud1km;
				
				var GMIdochogna1os=d.properties.dataGMIdochogna1os;
				
				var POWwydogna1os = d.properties.dataGMIPOWwydogna1os;			
				var GMIwydogna1os = d.properties.dataGMIwydogna1os;			
			
				var POWwydoswiwych=d.properties.dataGMIPOWwydoswiwych;
				var GMIwydoswiwych=d.properties.dataGMIwydoswiwych;
				
				if (POWnazwa!=GMInazwa) {
wysw='Powiat: '+POWnazwa+'<br>POW ludność: '+POWludog+' os<br>POW dochody na 1miesz. '+POWdochogna1os+' zł<br>POW wydatki na 1miesz. '+POWwydogna1os+
' zł<br>POW wydatki oświata i wych. '+POWwydoswiwych+' zł<br>Gmina: <span class="text-dark bg-light">&nbsp;'+GMInazwa +
'&nbsp;</span><br>Kod gminy: '+GMInr+'<br>GMI powierzchnia: '+GMIpow+' km<sup>2</sup><br>GMI ludność: '+GMIludog+
' os<br>GMI ludność: ' +GMIlud1km+ ' os/km<sup>2</sup>'+'<br>GMI dochody na 1miesz. '+GMIdochogna1os+' zł<br>GMI wydatki na 1miesz. '+GMIwydogna1os+
' zł<br>GMI wyd oświata i wych. '+GMIwydoswiwych;
		} else {
		wysw='Miasto na prawach powiatu<br>Gmina miejska: <span class="text-dark bg-light">&nbsp;'+POWnazwa+'&nbsp;</span><br>Kod gminy: '+GMInr+
		'<br>GMI powierzchnia: '+GMIpow+' km<sup>2</sup><br>GMI ludność: '+GMIludog+' os<br>GMI ludność: ' +GMIlud1km+ ' os/km<sup>2</sup>'+
		'<br>GMI dochody na 1miesz. '+GMIdochogna1os+' zł<br>GMI wydatki na 1miesz. '+GMIwydogna1os+' zł<br>GMI wyd oświata i wych. '+GMIwydoswiwych;
			}	
		}	
		
	if (!POWnazwa) wysw='Kod gminy: '+GMInr+'<br>Gmina: '+GMInazwa ;
				$("#etykieta").html(wysw).addClass("hover");				 				 
                });
                
       wojmap.on("mouseout", function() { 
            	  $("#etykieta").html('').removeClass("hover");			
        		});	
				
if (!formData) {				
$('#Radios1').prop("checked", true);		
}	

selectgmina();  

d2.resolve();
$( "#loading" ).css("display","none"); 
if (!startflag) {var top=$("#tytwoj").offset().top; $("html, body").animate({ scrollTop: top });}
startflag=false;
formData=false;
 
}, 'json'); 
  
if (kodwojnew && kodwojnew!='start') {kodwoj=kodwojnew;} 

kodwojnew=false;

});
  
}

else {
	$( "#loading" ).css("display","none");
	if (formData) {var top=$("#tytwoj").offset().top; $("html, body").animate({ scrollTop: top }); selectgmina(); formData=false;}
	}

	return false;
	}).submit();
	
	
$.when(d2).then(function() {
 $('#myradiobox').change(function(){
		$("#powiat").val('');
        var radselval = $("input[name='gridRadios']:checked").val();
        if (radselval=='gestosc') { 
		color.range(['#ffffe5','#fff7bc','#fee391','#fec44f','#fe9929','#ec7014','#cc4c02','#993404','#662506']); 
		color.domain([ minludnosc_na1km, maxludnosc_na1km]);
 
		paths.attr("fill", function(d) {   
								var value = d.properties.dataGMIlud1km; 
								
								if (value) { 
								return  color(value);} 
								else return "lightgray";
							 
						});
		}
		 if (radselval=='dochody') { 
		color.range(['#ffffe5','#fff7bc','#fee391','#fec44f','#fe9929','#ec7014','#cc4c02','#993404','#662506']); 
		color.domain([ mindochody_na1os, maxdochody_na1os]);
 
		paths.attr("fill", function(d) {   
								var value = d.properties.dataGMIdochogna1os; 
								
								if (value) { 
								return  color(value);} 
								else return "lightgray";
							 
						});
		}
			 if (radselval=='wydatki') { 
		color.range(['#ffffe5','#fff7bc','#fee391','#fec44f','#fe9929','#ec7014','#cc4c02','#993404','#662506']); 
		color.domain([ minwydatki_na1os, maxwydatki_na1os]);

		paths.attr("fill", function(d) {   
								var value = d.properties.dataGMIwydogna1os; 
								
								if (value) { 
								return  color(value);} 
								else return "lightgray";
							 
						});
		}
		
		}).change();
	
});

function selectgmina() {

if (formData) {

 $("input[name='gridRadios']:checked").prop("checked", false);

paths.attr("fill", function(d) { if (d.properties.JPT_KOD_JE==formData.kod) return 'blue';  
								else { return "lightgray";
							 }
						});


 }  

}  

function zoomFn() {
  d3.select('svg').select('g')
    .attr('transform', 'translate(' + d3.event.transform.x + ',' + d3.event.transform.y + ') scale(' + d3.event.transform.k + ')');
}

	
  }, 'json')


  